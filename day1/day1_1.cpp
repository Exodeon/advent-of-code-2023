#include <iostream>

int main() {
    std::string line;
    int sum = 0;
    while (std::cin >> line) {
        line.erase(std::remove_if(line.begin(), line.end(), [](unsigned char c) {return !std::isdigit(c);}), line.end());
        sum += std::stoi(std::string() + line[0] + line.back());
    }
    std::cout << sum << std::endl;
}