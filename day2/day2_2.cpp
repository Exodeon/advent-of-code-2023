#include <iostream>
#include <sstream>

int main() {
    std::string line;
    int powerSum = 0;
    while (getline(std::cin, line)) {
        // each game
        //std::cout << "whole line : " << line << std::endl;
        std::stringstream lineStream(line);
        std::string set;

        // getting the "Game X" and parsing the ID
        std::getline(lineStream, set, ':');
        std::stringstream gameStream(set);
        int gameId;
        gameStream >> set >> gameId;

        int redMax = 0;
        int greenMax = 0;
        int blueMax = 0;

        while (std::getline(lineStream, set, ';')) {
            // each set
            int red_count, green_count, blue_count = 0;
            std::stringstream setStream(set);
            std::string subset;
            while (std::getline(setStream, subset, ',')) {
                // each subset
                //std::cout << subset << std::endl;
                std::stringstream subsetStream(subset);
                int count;
                std::string color;
                subsetStream >> count >> color;

                // TODO : could be optimized using maps
                if (color == "blue" and count > blueMax) {
                    blueMax = count;
                } else if (color == "green" and count > greenMax) {
                    greenMax = count;
                } else if (color == "red" and count > redMax) {
                    redMax = count;
                }
            }
        }
        //std::cout << "red max = " << redMax << ", green max = " << greenMax << ", blue max = " << blueMax << std::endl;
        powerSum += redMax * greenMax * blueMax;
    }

    std::cout << "Sum of power : " << powerSum << std::endl;
}