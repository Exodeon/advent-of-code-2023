#include <iostream>
#include <sstream>

int main() {
    const int MAX_RED = 12;
    const int MAX_GREEN = 13;
    const int MAX_BLUE = 14;

    std::string line;
    int idSum = 0;
    while (getline(std::cin, line)) {
        // each game
        //std::cout << "whole line : " << line << std::endl;
        std::stringstream lineStream(line);
        std::string set;

        // getting the "Game X" and parsing the ID
        std::getline(lineStream, set, ':');
        std::stringstream gameStream(set);
        int gameId;
        gameStream >> set >> gameId;
        bool possible = true;
        while (std::getline(lineStream, set, ';') and possible) {
            // each set
            int red_count, green_count, blue_count = 0;
            std::stringstream setStream(set);
            std::string subset;
            while (std::getline(setStream, subset, ',') and possible) {
                // each subset
                //std::cout << subset << std::endl;
                std::stringstream subsetStream(subset);
                int count;
                std::string color;
                subsetStream >> count >> color;

                // TODO : could be optimized using maps
                if ((color == "blue" and count > MAX_BLUE) or (color == "red" and count > MAX_RED) or (color == "green" and count > MAX_GREEN)) {
                    possible = false;
                }
            }
        }
        if (possible) {
            idSum += gameId;
        }

    }

    std::cout << "Sum of IDs : " << idSum << std::endl;
}